\documentclass{article}
\usepackage[utf8]{inputenc}

\title{NF26 - Projet de Haute volumétrie}
\author{RUAN Qifeng, BRASSEUR Alexandre}

\usepackage{natbib}
\usepackage{graphicx}
\usepackage[french]{babel}
\selectlanguage{french}
\usepackage[T1]{fontenc}
\usepackage{minted}
\usepackage{hyperref}


\begin{document}
\maketitle


\section*{Introduction}

Ce projet a pour but de traiter, stocker et manipuler des données en haut volume. Pour cela, nous utilisons Cassandra, une base de données orientée colonne, pour le stockage et Python pour les traitements.


Les données en question sont des observations météorologiques provenant de capteurs ASOS sous le format METAR.
Le téléchargement des données sous format CSV et une description des variables sont disponibles ici:
\url{https://mesonet.agron.iastate.edu/request/download.phtml?network=IT__ASOS}

Notre traitement porte sur les données le réseaux ASOS d'Italie du 1er Janvier 2006 au 31 Décembre 2015. On remarque qu'il n'y a pas de données avant 2011, mais nous avons tout de même près de 4 millions de lignes.

Concernant le code, nous utilisons \texttt{plotly} et \texttt{matplotlib} pour les graphiques ainsi que \texttt{pandas} et \texttt{numpy} pour certains calculs.

Les fonctions seront notée \texttt{module.nom} avec le module python où la fonction se situe et son nom. Le script \texttt{test.py} donne un exemple de comment utiliser les modules. Les fonctions sont documentées dans le code. Chaque fonction de réponse produit un graphique récupérable dans le dossier \texttt{export}.

Le code est disponible sur le Gitlab de l'UTC, ici: \\
\url{https://gitlab.utc.fr/abrasseu/NF26-big-data}

\section{Modélisation et création des tables}

\subsection{Pré-traitement des données}

Chaque valeur de chaque ligne est formattée avec le bon type.
Afin d'avoir un meilleur contrôle sur la dimension temporelle des données, nous segmentons la colonne \texttt{valid} (TimeStamp) en \texttt{year}, \texttt{month}, \texttt{day}, \texttt{hour}, \texttt{minute}.
Les données sont lues et générées une par une via le générateur \texttt{data.get\_data}.


\subsection{Modélisation des tables}

Comme les 3 questions ont des besoins de partitionnement différents, nous créons des tables adaptées aux besoins.

\subsubsection{Table par station - \texttt{asos\_by\_station}}

Pour la première question, nous avons besoin d'un partitionnement par station.

Nous prenons alors les variables \texttt{(station, year)} comme clés de partitionnement. \texttt{station} car il faut obtenir l'historique pour une station donnée et \texttt{year} afin de mieux contrôler la taille des partitions de manière suffisante tout en ayant un nombre de requêtes à effectuer correct.

Nous prenons les variables \texttt{month, day, hour, minute} comme clés de tri, pour obtenir un tri respectant l'ordre temporel. Cela nous permet aussi d'agréger les données par jour afin de limiter la quantité de points afficher sur les graphiques.

La table obtenue est nommée \texttt{asos\_by\_station}.

\subsubsection{Table par temps - \texttt{asos\_by\_time}}

Pour les questions 2 et 3, c'est la dimension temporelle qui domine.

Nous prenons \texttt{(year, month, day)} comme clés de partitionnement et \texttt{hour, minute, station} comme clés de tri. Cela permet un contrôle suffisamment fin pour la question 2 et un traitement efficace de la période de la question 3.

La table obtenue est nommée \texttt{asos\_by\_time}.


\subsection{Remplissage des tables}

Afin de remplir efficacement les tables, nous lisons les données du CSV ligne par ligne et créons les requêtes d'insertions pour chaque table. Les variables \texttt{null} du CSV ne sont pas spécifiées dans ces requêtes, cela permet de diminuer la taille de celles-ci et Cassandra gère très bien cela. Les insertions sont effectuées par paquet (batch) de 60 afin de diminuer le nombre de requêtes envoyer à la base de données. La fonction \texttt{data.insert\_data} s'occupe de cela.





\section{Réponses aux questions}

\subsection{Question 1 - Historique et saisonnalité d'une station}

La fonction \texttt{scripts.plot\_station\_history(station, indicators)} permet d'afficher l'historique et la saisonnalité d'une station selon certains indicateurs.


Nous collectons d'abord les données depuis la table \texttt{asos\_by\_station} en sélectionnant  \texttt{year}, \texttt{month}, \texttt{day}, ainsi que les moyennes par jour des indicateurs sélectionnés pour la station désirée. Comme \texttt{year} est clé de partitionnement, nous effectuons une requête par année, de 2006 à 2015 pour nos données.
Nous récupérons les données d'intérêt pour l'affichage et calculons leur moyenne et écart-type par mois avec pandas. C'est efficace car les données agrégées par jour ne représentent pas une quantité énorme (3650 lignes pour 10 ans au plus).

Nous affichons alors les valeurs par une courbe bleue, la moyenne mensuelle (saisonnalité) en rouge et la moyenne plus ou moins l'écart-type en bande bleue.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Figures/history_LIRA.png}
    \caption{Historique et saisonnalité de la température et l'humidité de la station LIRA}
    \label{fig:history_LIRA}
\end{figure}

Nous observons bien la saisonnalité en rouge ainsi que les écarts grâce aux bandes décrites par l'écart-type en bleu transparent. Sur l'historique de l'humidité nous décernons quelques valeurs abbérantes autour de Février 2011 et Octobre 2012.


\subsection{Question 2 - Carte des indicateurs}

La fonction \texttt{scripts.plot\_indicators\_map(dt, indicators)} permet d'afficher une carte des station avec les valeurs des indicateurs sélectionnés à un temps précis.


Pour cette question nous utilisons la table \texttt{asos\_by\_time}.
Il s'agit alors simplement de récupérer les noms et coordonées spatiales des stations et les indicateurs sélectionnés pour l'instant demandé. Cela se fait bien grâce à la clé de partitionnement temporelle. On affiche ensuite les stations et leurs valeurs par indicateurs sur une carte via la fonction \texttt{save\_map} utilisant \texttt{plotly}. Nous obtenons le résultat suivant pour le 6 Janvier 2011:

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Figures/map.jpg}
    \caption{Carte de la température et de l'humidité le 06/02/2011}
    \label{fig:map}
\end{figure}

Les informations d'une station sont indiquées lorsque son point est survolé par la souris.



\subsection{Question 3 - Classification des stations}


La fonction \texttt{scripts.cluster\_stations(\_from, \_to, k, indicators)} \\
permet de classifier les stations selon en fonction des indicateurs sélectionnés sur une période définie.


Pour cette question nous utilisons aussi \texttt{asos\_by\_time}.
D'abord il faut récupérer des informations de forme stable pour chaque station. En effet selon la station nous n'avons pas les mêmes données disponibles. Pour comparer les stations avec une distance simple, nous calculons des statistiques par station, telles que la moyenne et la variance par indicateur choisi sur la période sélectionnée. Il faut ainsi itérer sur chaque donnée et calculer ces statistiques efficacement en stockant juste le nécessaire.

Ensuite nous pouvons appliquer la méthode des K-means avec une distance euclidienne pour chaque statistique de station. Il suffit ensuite de récupérer les groupes de chaque station et de les afficher sur une carte avec une couleur par groupe.


En classifiant les stations par température et humidité relative du 6 Janvier 2013 au 6 Avril 2014, nous obtenons les groupes suivants:

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Figures/kmeans.jpg}
    \caption{Classification des stations sur la période du 06/01/2013 au 06/04/2014}
    \label{fig:kmeans}
\end{figure}

Comme les centres ne sont pas des données géographiques, nous ne pouvons pas les afficher sur la carte.


\section{Conclusion}

Chacune des fonctions traite de manière efficace les données afin d'offrir une réponse le plus rapidement possible. Pour les questions 1 et 2 nous sommes bien en dessous de 5 secondes, même avec les 4 millions de données. Seule la question 3, prend plus de temps car elle doit itérer toutes les données de la période pour calculer des statistiques sur les indicateurs des stations. Une amélioration pourrait être de réduire la taille de l'itération en faisant une moyenne par jour des données lors de la requête CQL, voire de calculer directement les statistiques avec des agrégateurs personnalises défini dans Cassandra. Cependant le temps d'attente actuelle n'est de l'ordre que de quelques minutes, 5 tout au plus, ce qui est correct.
L'avantage globale est que chaque fonction est très flexible, permettant ainsi de choisir les indicateurs librement, avec uniquement des valeurs décimales pour les question 2 et 3.

\end{document}
