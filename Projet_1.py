import matplotlib.pyplot as plt
import datetime as dt
from cassandra.cluster import Cluster

cluster = Cluster(['localhost'])
session = cluster.connect('projet_td3_1920')

def Projet_1(session, station, parameter):
    #Draw a curve which shows the mean in eachday's parameter 
    queryAVG = 'SELECT station, year, month, AVG(' + parameter + ') AS PARA FROM projet_td3_1920.ASOS_Projet1 WHERE station = \'' + station + '\' GROUP BY station, year, month;'
    resultsAVG = session.execute(queryAVG)
    
    xAVG = []
    yAVG = []
    
    for row in resultsAVG:
        
        if(row.month < 10):
            date = '0' + str(row.month) + '/' + str(row.year) 
        else:
            date = str(row.month) + '/' + str(row.year)
        
        xAVG.append(dt.datetime.strptime(date,'%m/%Y').date())
        yAVG.append(row.para)
    
    #Draw a curve which shows the max in eachday's parameter 
    queryMAX = 'SELECT station, year, month, MAX(' + parameter + ') AS PARA FROM projet_td3_1920.ASOS_Projet1 WHERE station = \'' + station + '\' GROUP BY station, year, month;'
    resultsMAX = session.execute(queryMAX)
    
    xMAX = []
    yMAX = []
    
    for row in resultsMAX:

        if(row.month < 10):
            date = '0' + str(row.month) + '/' + str(row.year) 
        else:
            date = str(row.month) + '/' + str(row.year)
        
        xMAX.append(dt.datetime.strptime(date,'%m/%Y').date())
        yMAX.append(row.para)
    
    #Draw a curve which shows the min in eachday's parameter 
    queryMIN = 'SELECT station, year, month, MIN(' + parameter + ') AS PARA FROM projet_td3_1920.ASOS_Projet1 WHERE station = \'' + station + '\' GROUP BY station, year, month;'
    resultsMIN = session.execute(queryMIN)
    
    xMIN = []
    yMIN = []
    
    for row in resultsMIN:

        if(row.month < 10):
            date = '0' + str(row.month) + '/' + str(row.year) 
        else:
            date = str(row.month) + '/' + str(row.year)
        
        xMIN.append(dt.datetime.strptime(date,'%m/%Y').date())
        yMIN.append(row.para)
    
    plt.plot(xAVG, yAVG, 'r', xMAX, yMAX, 'b', xMIN, yMIN, 'b')
    plt.fill_between(xAVG, yMAX, yMIN, facecolor='blue')
    plt.xlabel('Temps')
    plt.ylabel(parameter)
    
    plt.show()
    plt.savefig('Projet1.png')

#station = 'LIBR'
#parameter = 'Air_Temperature'
#Projet_1(session, station, parameter)
station = 'LIRA'
parameter = 'Relative_Humidity'
Projet_1(session, station, parameter)
            
