from datetime import datetime
from typing import Sequence, Union
import plotly.graph_objs as pgo
import plotly as plty
import numpy as np
import re

YEARS = tuple(range(2006, 2015+1))
MAPBOX_ACCESS_TOKEN = 'pk.eyJ1IjoiYWJyYXNzZXUiLCJhIjoiY2p3c2NxZ2lzMXNseTRibGVqZ2tjNXluZSJ9.jwg0pnuR6330RlGwwkfq_Q'
NULL_VALUE = ''
RE_TS_PARSER = re.compile(r"(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+)(?: (?P<hour>\d+):(?P<minute>\d+))?.*")

def exclude_keys(data: dict, keys: Sequence[str]=[]):
	return { key: value for key, value in data.items()
											if key not in keys } if keys else data

def parse_dt_group(dt: Union[datetime, str], default=0):
	dt_str = dt.isoformat(sep=' ') if isinstance(dt, datetime) else dt
	dt_match = RE_TS_PARSER.match(dt_str)
	if not dt_match:
		raise ValueError("dt must be a valid timestamp")
	return { k: int(v) if v is not None else default for k, v in dt_match.groupdict().items() }

def dt_path(dt: datetime) -> str:
	dt = dt if isinstance(dt, datetime) else datetime.fromisoformat(dt)
	return dt.isoformat(sep='_').replace(':', '-')


def datetime_parser(value):
	return None if value == NULL_VALUE else datetime.fromisoformat(value)

def float_parser(value):
	return None if value == NULL_VALUE else float(value)

def int_parser(value):
	return None if value == NULL_VALUE else float(value)

def str_parser(value):
	return None if value == NULL_VALUE else value

# Cassandra Data Types
DATA_TYPES = {
	'station': 'TEXT',
	'valid': 'TIMESTAMP',

	'lat': 'DECIMAL',
	'lon': 'DECIMAL',
	'tmpf': 'DECIMAL',
	'dwpf': 'DECIMAL',
	'relh': 'DECIMAL',
	'drct': 'DECIMAL',
	'sknt': 'DECIMAL',
	'p01i': 'DECIMAL',
	'alti': 'DECIMAL',
	'mslp': 'DECIMAL',
	'vsby': 'DECIMAL',
	'gust': 'DECIMAL',

	'skyc1': 'TEXT',
	'skyc2': 'TEXT',
	'skyc3': 'TEXT',
	'skyc4': 'TEXT',
	'skyl1': 'TEXT',
	'skyl2': 'TEXT',
	'skyl3': 'TEXT',
	'skyl4': 'TEXT',
	'wxcodes': 'TEXT',
	'feel': 'DECIMAL',

	'ice_accretion_1hr': 'DECIMAL',
	'ice_accretion_3hr': 'DECIMAL',
	'ice_accretion_6hr': 'DECIMAL',
	'peak_wind_gust': 'DECIMAL',
	'peak_wind_drct': 'DECIMAL',

	'peak_wind_time': 'TIMESTAMP',
	'metar': 'TEXT',

	'year': 'INT',
	'month': 'INT',
	'day': 'INT',
	'hour': 'INT',
	'minute': 'INT',
}

PARSERS_MAP = {
	'DECIMAL STATIC': float_parser,
	'DECIMAL': float_parser,
	'INT': int_parser,
	'TEXT': str_parser,
	'TIMESTAMP': str_parser,
}

INDICATORS_NAMES = {
	'station': 'Station',
	'valid': 'Temps',
	'lat': 'Latitude',
	'lon': 'Longitude',

	'tmpf': 'Température',
	'dwpf': 'Point de rosée',
	'relh': 'Humidité relative',
	'drct': 'Direction du vent',
	'sknt': 'Vitesse du vent',
	'p01i': 'Précipitations',
	'alti': 'Altimètre de pression',
	'mslp': 'Pression au niveau de la mer',
	'vsby': 'Visibilité',
	'gust': 'Rafale de vent',

	'skyc1': 'Couverture 1 du ciel',
	'skyc2': 'Couverture 2 du ciel',
	'skyc3': 'Couverture 3 du ciel',
	'skyc4': 'Couverture 4 du ciel',
	'skyl1': 'Altitude de la couverture 1 ciel',
	'skyl2': 'Altitude de la couverture 2 ciel',
	'skyl3': 'Altitude de la couverture 3 ciel',
	'skyl4': 'Altitude de la couverture 4 ciel',
	'wxcodes': 'Codes météorologiques',
	'feel': 'Température ressentie',

	'ice_accretion_1hr': 'Accumulation de glace (1h)',
	'ice_accretion_3hr': 'Accumulation de glace (3h)',
	'ice_accretion_6hr': 'Accumulation de glace (6h)',
	'peak_wind_gust': 'Rafale de vent maximale',
	'peak_wind_drct': 'Direction de la rafale de vent maximale',
	'peak_wind_time': 'Temps de la rafale de vent maximale',

	'metar': 'Code METAR',
}

def save_map(lat_list: Sequence, lon_list: Sequence, text_list: Sequence,
						 save_path: str, title: str=None, by_group: bool=False):

	smb_options = {
		'mode': 'markers',
		'hoverinfo': 'text',
		'marker': pgo.scattermapbox.Marker(size=12),
	}

	# Create map
	if by_group:
		data = []
		for lat, lon, text in zip(lat_list, lon_list, text_list):
			data.append(
				pgo.Scattermapbox(lat=lat, lon=lon, text=text, **smb_options)
			)
	else:
		data = [
			pgo.Scattermapbox(lat=lat_list, lon=lon_list, text=text_list, **smb_options),
		]

	layout = pgo.Layout(
		autosize=True,
		hovermode='closest',
		title=title,
		mapbox=pgo.layout.Mapbox(
				accesstoken=MAPBOX_ACCESS_TOKEN,
				center=pgo.layout.mapbox.Center(lat=42, lon=12.5),
				bearing=0,
				zoom=4.5,
				pitch=0,
		),
	)

	# Export map
	fig = pgo.Figure(data=data, layout=layout)
	plty.offline.plot(fig, filename=save_path, auto_open=False)
	print(f"Saved map at {save_path}")
