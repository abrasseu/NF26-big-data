from utils import (
	PARSERS_MAP, DATA_TYPES, RE_TS_PARSER,
	exclude_keys,
)
from cassandra.cluster import Cluster
from typing import Sequence, Dict
from tqdm.auto import tqdm
import itertools
import csv


# Default config
FILE_PATH = './asos.txt'
DEFAULT_KEYSPACE = 'abrasseu_projet_1920'
TABLES_CONFIG = {
	'asos_by_station': {
		'primary_key': '((station, year), month, day, hour, minute)',
		# 'exclude_keys': tuple(),
		# 'additional_keys': {},
		# 'data_mapper': decompose_valid_with,
	},
	'asos_by_time': {
		'primary_key': '((year, month, day), hour, minute, station)',
	# 	'exclude_keys': ('valid', ),
	# 	'additional_keys': {},
	# 	'data_mapper': decompose_valid,
	},
}

# ====================================================================
# 		Database
# ====================================================================

def init_database(keyspace: str=DEFAULT_KEYSPACE, tables_config: dict=TABLES_CONFIG):
	"""
	Initialize the Cassandra keyspace and tables
	"""
	cluster = Cluster(['localhost'])
	session = cluster.connect()

	print("Making sure the keyspace exists...")
	session.execute(f"CREATE KEYSPACE IF NOT EXISTS {keyspace} "
									+"WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '2' };")
	session.set_keyspace(keyspace)

	print("Dropping and recreating tables...")
	for name, options in tables_config.items():
		session.execute(f"DROP TABLE IF EXISTS {name};")
		session.execute(create_table_query(name, options))

	print("Database initialized.")
	return session

def get_db_session(keyspace: str=DEFAULT_KEYSPACE):
	"""Get the Cassandra database session"""
	cluster = Cluster(['localhost'])
	try:
		return cluster.connect(keyspace)
	except:
		return init_database(keyspace)


# ====================================================================
# 		Queries
# ====================================================================
def create_table_query(table_name: str, config: dict, remove_valid: bool=True):
	"""
	Create a Cassandra table

	@param      primary_key       The primary key for the table
	@param      exclude_keys      Default types to exclude (default: [])
	@param      additional_keys   Additional keys with type to include (default: {})

	@return     The CREATE TABLE query
	"""
	_exclude_keys = config.get('exclude_keys', ('valid',) if remove_valid else tuple())
	additional_keys = config.get('additional_keys', {})

	keys_gen = itertools.chain(DATA_TYPES.items(), additional_keys.items())
	key_types = ',\n'.join(f"\t\t\t{key} {dtype}" for key, dtype in keys_gen
																								if key not in _exclude_keys) 
	return f"""
		CREATE TABLE IF NOT EXISTS {table_name} (
			{key_types},
			PRIMARY KEY {config['primary_key']}
		);
	"""

def insert_row_query(table_name: str, data: dict):
	keys, values = [], []
	for key, value in data.items():
		if value is not None:
			keys.append(key)
			values.append(f"'{value}'" if type(value) == str else str(value))
	return f"INSERT INTO {table_name} ({', '.join(keys)}) VALUES ({', '.join(values)});"


# ====================================================================
# 		Data
# ====================================================================

def get_data(path: str=FILE_PATH, limit: int=None, remove_valid: bool=True, offset: int=None):
	"""
	Read and yield data from the downloaded CSV file
	"""
	with open(path) as file:
		gen = csv.DictReader(file)
		if offset:
			gen = itertools.islice(gen, offset, None)

		for i, row in enumerate(gen):
			if limit and i >= limit:
				break
			# Parse line
			data = { key: PARSERS_MAP[DATA_TYPES[key]](value) for key, value in row.items() }
			# Parse timestamp
			match = RE_TS_PARSER.match(data['valid'])
			if match:
				data.update({ k: int(v) for k, v in match.groupdict().items() })
				if remove_valid:
					del data['valid']
				yield data
			else:
				yield None


def flexible_insert_data(session, data_gen, batch_size: int=50, **kwargs):
	"""
	Insert data in Cassandra database by batch
	
	@param      session        The Cassandra session
	@param      data_gen       The data generator
	@param      tables_config  The configurations of the tables to insert the data into
	@param      batch_size     The batch size (default: 100)
	"""
	tables_config = kwargs.get('tables_config', TABLES_CONFIG)
	skipped = 0

	# Insert data by batch
	statements = { name: 'BEGIN BATCH\n' for name in tables_config }
	for i, data in enumerate(tqdm(data_gen, desc='Inserting data', total=kwargs.get('limit'))):
		if not data:
			skipped += 1
			continue

		# Add insert row for each table
		for name, config in tables_config.items():
			if config.get('data_mapper'):
				_data = config['data_mapper'](data)
			elif config.get('exclude_keys'):
				_data = exclude_keys(data, config['exclude_keys'])
			else:
				_data = data

			statements[name] += f"\t{insert_row_query(name, data)}\n"

		# Launch batch for each table
		if i % batch_size == 0 and i != 0:
			for name in tables_config:
				session.execute(statements[name] + 'APPLY BATCH;')
				statements[name] = 'BEGIN BATCH\n'


	# If there data remaining, apply the batch
	if i % batch_size != 0:
		for name in tables_config:
			session.execute(statements[name] + 'APPLY BATCH;')

	print(f"Inserted {i+1} and skipped {skipped} rows.")


def insert_data(session, data_gen, batch_size: int=50, **kwargs):
	"""
	Insert data in Cassandra database by batch
	
	@param      session        The Cassandra session
	@param      data_gen       The data generator
	@param      tables_config  The configurations of the tables to insert the data into
	@param      batch_size     The batch size (default: 100)
	"""
	tables_config = kwargs.get('tables_config', TABLES_CONFIG)
	skipped = 0

	TEMPLATE_NAME = '<tn>'

	# Insert data by batch
	statement = 'BEGIN BATCH\n'
	for i, data in enumerate(tqdm(data_gen, desc='Inserting data', total=kwargs.get('limit'))):
		if not data:
			skipped += 1
			continue

		statement += f"\t{insert_row_query(TEMPLATE_NAME, data)}\n"

		# Launch batch for each table
		if i % batch_size == 0 and i != 0:
			for name in tables_config:
				session.execute(statement.replace(TEMPLATE_NAME, name) + 'APPLY BATCH;')
			statement = 'BEGIN BATCH\n'


	# If there data remaining, apply the batch
	if i % batch_size != 0:
		for name in tables_config:
			session.execute(statement.replace(TEMPLATE_NAME, name) + 'APPLY BATCH;')

	print(f"Inserted {i+1} and skipped {skipped} rows.")
