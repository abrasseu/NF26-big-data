from data import *
from scripts import *
from pprint import pprint as pp


# init_database()

# OFFSET = None
# LIMIT = None
# session = get_db_session()
# data_gen = get_data(limit=LIMIT, offset=OFFSET)
# insert_data(session, data_gen, batch_size=60, limit=(LIMIT if LIMIT else int(6e6)))

try:
	from IPython import get_ipython
	ipython = get_ipython()
	print("Using ipython to time methods")
	ipython.magic("time plot_station_history('LIRA')")
	ipython.magic("time plot_indicators_map('2011-02-06')")
	ipython.magic("time cluster_stations('2013-02-06', '2014-07-06')")
except AttributeError:
	print("Fallback to classic python")
	plot_station_history('LIRA')
	plot_indicators_map('2011-02-06')
	cluster_stations('2013-02-06', '2013-09-06')
