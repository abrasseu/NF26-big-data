import matplotlib.pyplot as plt
from datetime import datetime, date
from typing import Sequence, Union
from tqdm.auto import tqdm
import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

from data import get_db_session
from utils import (
	INDICATORS_NAMES, MAPBOX_ACCESS_TOKEN, RE_TS_PARSER,
	DATA_TYPES, YEARS, parse_dt_group, save_map, dt_path
)

session = get_db_session()


# Objective 1
def plot_station_history(station: str, indicators: Sequence[str]=('tmpf',), **kwargs):
	"""
	Pour un point donné de l’espace, je veux pouvoir avoir un historique du
	passé, avec des courbes adaptés. Je vous pouvoir mettre en évidence la
	saisonnalité et les écarts à la saisonnalité.
	"""
	indicators = tuple(indicators)
	if any(DATA_TYPES[indic] != 'DECIMAL' for indic in indicators):
		raise ValueError("indicators must be numerical values")

	save_path = kwargs.get('save_path', f"./exports/history_{station}.png")
	indicators_selected = ', '.join(f"AVG({i}) AS {i}" for i in indicators)

	# Get station data
	base_query = f"SELECT year, month, day, {indicators_selected} FROM asos_by_station" \
						 + f" WHERE station='{station}' AND year=<year>" \
						 + f" GROUP BY year, month, day"

	def row_gen():
		for year in YEARS:
			query = base_query.replace('<year>', str(year))
			yield from session.execute(query)

	# Process results
	x = []
	y = { indicator: [] for indicator in indicators }
	print("Fetching data...")
	for row in row_gen():
		# Get day data and compute seasonality
		x.append(date(row.year, row.month, row.day))
		for indicator in indicators:
			y[indicator].append(getattr(row, indicator))

	# Compute seasonality with pandas
	print("Computing seasonality...")
	x = pd.DatetimeIndex(x)
	y = pd.DataFrame(y, index=x, dtype=float)
	monthly_y = y.groupby([ x.year, x.month ])
	seasonal_x = pd.to_datetime([ date(*key, 1) for key in monthly_y.groups.keys() ])
	seasonal_mean = monthly_y.mean()
	seasonal_std = monthly_y.std()

	# Display results
	print("Display results...")
	n = len(indicators)
	fig, axes = plt.subplots(nrows=n, figsize=(20, 5*n))
	for ax, indicator in zip(axes if n > 1 else [axes], indicators):
		ax.plot(x, y[indicator], 'b')

		s_m = seasonal_mean[indicator]
		ax.plot(seasonal_x, s_m, 'r-')
		ax.fill_between(seasonal_x, s_m + seasonal_std[indicator], s_m - seasonal_std[indicator])

		ax.set_title(f"Historique de {INDICATORS_NAMES[indicator]} pour la station {station}")
		ax.set_ylabel(indicator)

	plt.show()
	plt.savefig(save_path)
	print(f"Saved graph at {save_path}")


# Objective 2
def plot_indicators_map(dt: Union[datetime, str], indicators: Sequence[str]=('tmpf', 'relh'), **kwargs):
	"""
	À un instant donné je veux pouvoir obtenir une carte me représentant
	n’importe quel indicateur.
	"""
	dt_groups = parse_dt_group(dt, None)
	condition = ' AND '.join(f"{key}={value}" for key, value in dt_groups.items() if value is not None)

	indicators = tuple(i for i in indicators if i not in ('lat', 'lon', 'station'))
	save_path = kwargs.get('save_path', f"./exports/map_{dt_path(dt)}.html")

	query = f"SELECT lat, lon, station, {', '.join(indicators)} FROM asos_by_time WHERE {condition};"
	results = session.execute(query)

	# Process results
	print("Fetching data...")
	lat_list, lon_list, hovertext = [], [], []
	for row in results:
		lat_list.append(row.lat)
		lon_list.append(row.lon)
		text = f"<b>{row.station}</b> ({row.lat:.2f}, {row.lon:.2f})<br />"
		for indic in indicators:
			text += f"<br />{indic:<8}: {getattr(row, indic)}"
		hovertext.append(text)

	if len(lat_list) == 0:
		print(f"No data for {dt}")
		return None

	title = f"Carte des indicateurs {indicators} à l'instant {dt}"
	save_map(lat_list, lon_list, hovertext, save_path, title)


# Objective 3
def cluster_stations(_from: datetime, _to: datetime, k: int=3,
										 indicators: Sequence[str]=('tmpf', 'relh'), **kwargs):
	"""
	Pour une période de temps donnée, je veux pouvoir obtenir clusteriser
	l’espace, et représenter cette clusterisation.
	"""
	indicators = tuple(indicators)
	if any(DATA_TYPES[indic] != 'DECIMAL' for indic in indicators):
		raise ValueError("indicators must be numerical values")

	# === Params

	_from = _from if isinstance(_from, datetime) else datetime.fromisoformat(_from)
	_to   = _to   if isinstance(_to  , datetime) else datetime.fromisoformat(_to  )
	dt_range = pd.date_range(start=_from.date(), end=_to.date())
	tol = kwargs.get('tol', 1e-4)
	save_path = kwargs.get('save_path', f"./exports/clust_{dt_path(_from)}_{dt_path(_to)}.html")

	# Base query
	base_query = f"SELECT station, lat, lon, {', '.join(indicators)} FROM asos_by_time" \
							+ " WHERE year=%d AND month=%d AND day=%d"

	# Row generator
	def row_gen():
		last = len(dt_range) - 1
		for i, dt in enumerate(dt_range):
			query = base_query % (dt.year, dt.month, dt.day)
			if i == 0 and (_from.hour != 0 or _from.minute != 0):
					query += f" AND hour>={_from.hour} AND minute>={_from.minute}"
			elif i == last and (_to.hour != 0 or _to.minute != 0):
					query += f" AND hour<={_to.hour} AND minute<={_to.minute}"
			query += ';'
			yield from session.execute(query)

	# === Stats

	stats_names = ('moy', 'var')
	n_stats = len(stats_names)
	stats = {}
	lat_lon = {}

	# Compute stats on rows
	print("Processing data...")
	for row in tqdm(row_gen()):
		if row.station not in stats:
			stats[row.station] = np.zeros(n_stats * len(indicators) + 1)

		if row.station not in lat_lon:
			lat_lon[row.station] = (row.lat, row.lon)

		for i, indic in enumerate(indicators):
			value = getattr(row, indic, None)
			value = 0 if value is None else float(value)
			stats[row.station][n_stats*i] += value
			stats[row.station][n_stats*i+1] += value**2
		stats[row.station][-1] += 1

	# Finalize stats computation
	for station in stats:
		count = stats[station][-1]
		for i in range(len(indicators)):
			if count > 1:
				stats[station][n_stats*i+1] = (stats[station][n_stats*i+1] - stats[station][n_stats*i]**2/count) / (count - 1)
			else:
				stats[station][n_stats*i+1] = 0
			stats[station][n_stats*i] /= count
		stats[station] = stats[station][:-1]

	# === k-means

	print("Applying k-means algorithm...")
	for run in range(kwargs.get('nb_max_run', 3)):
		# Choose initial centroids
		if 'initial_centroids' in kwargs:
			centroids = np.array(kwargs['initial_centroids'])
		else:
			init_keys = np.random.choice(tuple(stats.keys()), k)
			centroids = np.array([ stats[key] for key in init_keys ])

		# Compute class centers
		for ite in range(kwargs.get('nb_max_ite', 10)):

			# Compute new centroids
			count_classes = np.zeros(k)
			centroids_acc = np.zeros((k, len(indicators) * n_stats))
			for station, row in stats.items():
				# Compute class
				dist_from_centroids = tuple( np.linalg.norm(centroid - row) for centroid in centroids )
				class_index = np.argmin(dist_from_centroids)

				# Compute new centroids
				count_classes[class_index] += 1
				centroids_acc[class_index] += row
				
			# Update centroids
			old_centroids = centroids
			count_classes[count_classes == 0] = 1
			centroids = centroids_acc / count_classes[:, None]

			# Check convergence
			if np.abs(centroids - old_centroids).sum() < tol:
				break

		# Check if no class has disappeared
		if (count_classes > 0).all():
			break

	# Compute final classes
	lat_list  = [ list() for _ in range(k) ]
	lon_list  = [ list() for _ in range(k) ]
	text_list = [ list() for _ in range(k) ]
	for station, row in stats.items():
		# Get class
		dist_from_centroids = tuple( np.linalg.norm(centroid - row) for centroid in centroids )
		class_index = np.argmin(dist_from_centroids)
		print(class_index)

		# Get text
		lat, lon = lat_lon[station]
		text = f"<b>{station}</b> ({lat:.2f}, {lon:.2f})<br />"
		for i, indic in enumerate(indicators):
			text += f"<br />{indic:<8}:"
			for j, stat in enumerate(stats_names):
				text += f"<br /> - {stat:<6}: {stats[station][n_stats*i+j]}"

		# Add info to class lists
		lat_list[class_index].append(lat)
		lon_list[class_index].append(lon)
		text_list[class_index].append(text)

	# === Display results

	title = f"Classification par {indicators} sur la période {_from} - {_to}"
	return save_map(lat_list, lon_list, text_list, save_path, title, by_group=True)
